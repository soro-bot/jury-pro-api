-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 25, 2021 at 01:06 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `akil`
--
CREATE DATABASE IF NOT EXISTS `akil` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `akil`;
--
-- Database: `api_rest_lara`
--
CREATE DATABASE IF NOT EXISTS `api_rest_lara` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `api_rest_lara`;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `nom`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Data', 'Technologie', 'Success', '2020-09-29 10:01:06', '2020-09-29 10:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_09_28_183734_create_jobs_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Database: `boutique`
--
CREATE DATABASE IF NOT EXISTS `boutique` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `boutique`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_06_25_160141_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `boutique_api`
--
CREATE DATABASE IF NOT EXISTS `boutique_api` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `boutique_api`;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_07_09_104039_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'First Post', 'This the First Post body', '2020-07-09 10:53:59', '2020-07-09 10:54:00'),
(2, 'First Post', 'This is the First Post  body\r\n', '2020-07-09 11:18:14', '2020-07-09 11:18:14'),
(3, 'Third Post', 'This is the Third Post', '2020-07-09 13:49:22', '2020-07-09 13:49:22'),
(4, 'Third Post', 'This is the Third Post', '2020-07-09 13:51:30', '2020-07-09 13:51:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `bow_api`
--
CREATE DATABASE IF NOT EXISTS `bow_api` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bow_api`;
--
-- Database: `dbgreentrash`
--
CREATE DATABASE IF NOT EXISTS `dbgreentrash` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `dbgreentrash`;

-- --------------------------------------------------------

--
-- Table structure for table `activites`
--

CREATE TABLE `activites` (
  `idactivite` int(10) UNSIGNED NOT NULL,
  `idequipement` int(10) UNSIGNED NOT NULL,
  `niveau` double(8,2) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations` (
  `idconfig` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seuil` double NOT NULL,
  `longueur` double NOT NULL,
  `periodicite` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`idconfig`, `libelle`, `seuil`, `longueur`, `periodicite`, `created_at`, `updated_at`) VALUES
(1, 'default', 88.9, 45, 1, '2020-08-26 16:08:41', '2020-08-26 16:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `equipements`
--

CREATE TABLE `equipements` (
  `idequipement` int(10) UNSIGNED NOT NULL,
  `numequipement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capteur` tinyint(1) NOT NULL,
  `gps` tinyint(1) NOT NULL,
  `veille` tinyint(1) NOT NULL,
  `idconfig` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equipements`
--

INSERT INTO `equipements` (`idequipement`, `numequipement`, `capteur`, `gps`, `veille`, `idconfig`, `created_at`, `updated_at`) VALUES
(1, '45236088', 1, 1, 0, 1, '2020-08-26 16:09:59', '2020-08-26 16:09:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_08_25_114415_create_roles_table', 1),
(2, '2020_08_25_114835_create_zones_table', 1),
(3, '2020_08_25_115014_create_utilisateurs_table', 1),
(4, '2020_08_25_133220_create_configurations_table', 1),
(5, '2020_08_25_133549_create_equipements_table', 1),
(6, '2020_08_25_134251_create_poubelles_table', 1),
(7, '2020_08_25_141214_create_activites_table', 1),
(8, '2020_08_25_141528_create_vehicules_table', 1),
(9, '2020_08_25_142114_create_missions_table', 1),
(10, '2020_08_25_143545_create_utilisateurs_missions_table', 1),
(11, '2020_08_25_143731_create_missions_poubelles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `missions`
--

CREATE TABLE `missions` (
  `idmission` int(10) UNSIGNED NOT NULL,
  `idvehicule` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateExecution` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `datefin` date DEFAULT NULL,
  `datedemande` date DEFAULT NULL,
  `confirme` tinyint(1) NOT NULL,
  `responsable` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `missions`
--

INSERT INTO `missions` (`idmission`, `idvehicule`, `libelle`, `dateExecution`, `description`, `datefin`, `datedemande`, `confirme`, `responsable`, `created_at`, `updated_at`) VALUES
(1, 1, 'Marcory', '2020-08-27', 'Ramassage dans la zone de Marcory', '2020-08-27', '2020-08-26', 1, 1, '2020-08-26 16:31:40', '2020-08-26 17:06:56'),
(3, 1, 'Treichville', '2020-08-28', 'Ramassage dans la zone de Treichville', '2020-08-28', '2020-08-28', 1, 1, '2020-08-27 10:51:37', '2020-08-27 10:51:37'),
(4, 1, 'Treichville', '2020-08-28', 'Ramassage dans la zone de Treichville', '2020-08-28', '2020-08-28', 0, 1, '2020-08-28 11:38:22', '2020-08-28 11:38:22'),
(5, 1, 'Treichville', '2020-08-28', 'Ramassage dans la zone de Treichville', '2020-08-28', '2020-08-28', 0, 1, '2020-08-28 11:38:42', '2020-08-28 11:38:42'),
(6, 1, 'Treichville', '2020-08-28', 'Ramassage dans la zone de Treichville', '2020-08-28', '2020-08-28', 0, 1, '2020-08-28 11:40:14', '2020-08-28 11:40:14'),
(7, 1, 'Treichville', '2020-08-28', 'Ramassage dans la zone de Treichville', '2020-08-28', '2020-08-28', 0, 1, '2020-08-28 11:41:49', '2020-08-28 11:41:49'),
(8, 1, 'Treichville', '2020-08-28', 'Ramassage dans la zone de Treichville', '2020-08-28', '2020-08-28', 1, 1, '2020-08-28 11:44:33', '2020-08-28 11:54:20');

-- --------------------------------------------------------

--
-- Table structure for table `missions_poubelles`
--

CREATE TABLE `missions_poubelles` (
  `id` int(10) UNSIGNED NOT NULL,
  `idpoubelle` int(10) UNSIGNED NOT NULL,
  `idmission` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `poubelles`
--

CREATE TABLE `poubelles` (
  `idpoubelle` int(10) UNSIGNED NOT NULL,
  `idzone` int(10) UNSIGNED NOT NULL,
  `idequipement` int(11) DEFAULT NULL,
  `numeroPoubelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `poubelles`
--

INSERT INTO `poubelles` (`idpoubelle`, `idzone`, `idequipement`, `numeroPoubelle`, `description`, `created_at`, `updated_at`) VALUES
(4, 1, 1, '78429087', 'ECO', '2020-08-26 16:05:33', '2020-08-28 12:19:46'),
(5, 2, 1, '09090077', 'ECOSA', '2020-08-27 09:52:49', '2020-08-27 09:52:49');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `idrole` int(10) UNSIGNED NOT NULL,
  `libellerole` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `iduser` int(10) UNSIGNED NOT NULL,
  `idrole` int(10) UNSIGNED NOT NULL,
  `idzone` int(10) UNSIGNED NOT NULL,
  `matricule` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datenaiss` date NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mdp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lieuResidence` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utilisateurs_missions`
--

CREATE TABLE `utilisateurs_missions` (
  `id` int(10) UNSIGNED NOT NULL,
  `iduser` int(10) UNSIGNED NOT NULL,
  `idmission` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicules`
--

CREATE TABLE `vehicules` (
  `idvehicule` int(10) UNSIGNED NOT NULL,
  `immatriculation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modele` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `couleur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idequipement` int(11) DEFAULT NULL,
  `idzone` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicules`
--

INSERT INTO `vehicules` (`idvehicule`, `immatriculation`, `modele`, `marque`, `couleur`, `idequipement`, `idzone`, `created_at`, `updated_at`) VALUES
(1, 'D19 000 ', 'V9', 'Toyota', 'Orange', 1, 2, '2020-08-26 16:24:30', '2020-08-27 09:19:54'),
(3, 'D24 986', 'ECOSA', 'Mercedes', 'Vert', 1, 1, '2020-08-27 09:41:32', '2020-08-27 09:41:32');

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `idzone` int(10) UNSIGNED NOT NULL,
  `libellezone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idzoneparent` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`idzone`, `libellezone`, `idzoneparent`, `created_at`, `updated_at`) VALUES
(1, 'Abidjan', NULL, '2020-08-26 16:06:33', '2020-08-26 16:06:33'),
(2, 'Yopougon', 1, '2020-08-26 16:06:33', '2020-08-26 16:06:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activites`
--
ALTER TABLE `activites`
  ADD PRIMARY KEY (`idactivite`),
  ADD KEY `activites_idequipement_foreign` (`idequipement`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`idconfig`);

--
-- Indexes for table `equipements`
--
ALTER TABLE `equipements`
  ADD PRIMARY KEY (`idequipement`),
  ADD KEY `equipements_idconfig_foreign` (`idconfig`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `missions`
--
ALTER TABLE `missions`
  ADD PRIMARY KEY (`idmission`),
  ADD KEY `missions_idvehicule_foreign` (`idvehicule`);

--
-- Indexes for table `missions_poubelles`
--
ALTER TABLE `missions_poubelles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `missions_poubelles_idpoubelle_foreign` (`idpoubelle`),
  ADD KEY `missions_poubelles_idmission_foreign` (`idmission`);

--
-- Indexes for table `poubelles`
--
ALTER TABLE `poubelles`
  ADD PRIMARY KEY (`idpoubelle`),
  ADD UNIQUE KEY `poubelles_numeropoubelle_unique` (`numeroPoubelle`),
  ADD KEY `poubelles_idzone_foreign` (`idzone`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idrole`);

--
-- Indexes for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `utilisateurs_matricule_unique` (`matricule`),
  ADD UNIQUE KEY `utilisateurs_login_unique` (`login`),
  ADD KEY `utilisateurs_idrole_foreign` (`idrole`),
  ADD KEY `utilisateurs_idzone_foreign` (`idzone`);

--
-- Indexes for table `utilisateurs_missions`
--
ALTER TABLE `utilisateurs_missions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utilisateurs_missions_iduser_foreign` (`iduser`),
  ADD KEY `utilisateurs_missions_idmission_foreign` (`idmission`);

--
-- Indexes for table `vehicules`
--
ALTER TABLE `vehicules`
  ADD PRIMARY KEY (`idvehicule`),
  ADD UNIQUE KEY `vehicules_immatriculation_unique` (`immatriculation`),
  ADD KEY `vehicules_idzone_foreign` (`idzone`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`idzone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activites`
--
ALTER TABLE `activites`
  MODIFY `idactivite` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configurations`
--
ALTER TABLE `configurations`
  MODIFY `idconfig` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `equipements`
--
ALTER TABLE `equipements`
  MODIFY `idequipement` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `missions`
--
ALTER TABLE `missions`
  MODIFY `idmission` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `missions_poubelles`
--
ALTER TABLE `missions_poubelles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `poubelles`
--
ALTER TABLE `poubelles`
  MODIFY `idpoubelle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `idrole` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `iduser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utilisateurs_missions`
--
ALTER TABLE `utilisateurs_missions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicules`
--
ALTER TABLE `vehicules`
  MODIFY `idvehicule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `idzone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activites`
--
ALTER TABLE `activites`
  ADD CONSTRAINT `activites_idequipement_foreign` FOREIGN KEY (`idequipement`) REFERENCES `equipements` (`idequipement`);

--
-- Constraints for table `equipements`
--
ALTER TABLE `equipements`
  ADD CONSTRAINT `equipements_idconfig_foreign` FOREIGN KEY (`idconfig`) REFERENCES `configurations` (`idconfig`);

--
-- Constraints for table `missions`
--
ALTER TABLE `missions`
  ADD CONSTRAINT `missions_idvehicule_foreign` FOREIGN KEY (`idvehicule`) REFERENCES `vehicules` (`idvehicule`);

--
-- Constraints for table `missions_poubelles`
--
ALTER TABLE `missions_poubelles`
  ADD CONSTRAINT `missions_poubelles_idmission_foreign` FOREIGN KEY (`idmission`) REFERENCES `missions` (`idmission`),
  ADD CONSTRAINT `missions_poubelles_idpoubelle_foreign` FOREIGN KEY (`idpoubelle`) REFERENCES `poubelles` (`idpoubelle`);

--
-- Constraints for table `poubelles`
--
ALTER TABLE `poubelles`
  ADD CONSTRAINT `poubelles_idzone_foreign` FOREIGN KEY (`idzone`) REFERENCES `zones` (`idzone`);

--
-- Constraints for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `utilisateurs_idrole_foreign` FOREIGN KEY (`idrole`) REFERENCES `roles` (`idrole`),
  ADD CONSTRAINT `utilisateurs_idzone_foreign` FOREIGN KEY (`idzone`) REFERENCES `zones` (`idzone`);

--
-- Constraints for table `utilisateurs_missions`
--
ALTER TABLE `utilisateurs_missions`
  ADD CONSTRAINT `utilisateurs_missions_idmission_foreign` FOREIGN KEY (`idmission`) REFERENCES `missions` (`idmission`),
  ADD CONSTRAINT `utilisateurs_missions_iduser_foreign` FOREIGN KEY (`iduser`) REFERENCES `utilisateurs` (`iduser`);

--
-- Constraints for table `vehicules`
--
ALTER TABLE `vehicules`
  ADD CONSTRAINT `vehicules_idzone_foreign` FOREIGN KEY (`idzone`) REFERENCES `zones` (`idzone`);
--
-- Database: `demo`
--
CREATE DATABASE IF NOT EXISTS `demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `demo`;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `email`, `first_name`, `last_name`) VALUES
(11, 'john@gmail.com', 'John', 'john'),
(12, 'ramesh@gmail.com', 'Ramesh', 'Fadatare'),
(13, 'tom@gmail.com', 'Tom', 'Cruise'),
(14, 'admin@gmail.com', 'Admin', 'admin'),
(15, 'ram@gmail.com', 'Ram', 'Cruise'),
(16, 'sanjay@gmail.com', 'Sanjay', 'Pawar');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Database: `forge`
--
CREATE DATABASE IF NOT EXISTS `forge` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `forge`;
--
-- Database: `jury_pro`
--
CREATE DATABASE IF NOT EXISTS `jury_pro` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `jury_pro`;

-- --------------------------------------------------------

--
-- Table structure for table `candidats`
--

CREATE TABLE `candidats` (
  `candidat_id` int(11) NOT NULL,
  `candidat_code` int(11) NOT NULL,
  `candidat_email` varchar(255) DEFAULT NULL,
  `candidat_nom` varchar(255) DEFAULT NULL,
  `candidat_photo` tinyblob,
  `candidat_prenom` varchar(255) DEFAULT NULL,
  `candidat_telephone` int(11) NOT NULL,
  `evenement_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `candidats`
--

INSERT INTO `candidats` (`candidat_id`, `candidat_code`, `candidat_email`, `candidat_nom`, `candidat_photo`, `candidat_prenom`, `candidat_telephone`, `evenement_id`) VALUES
(1, 12, 'germain.ik@gmail.com', 'Xamiver', NULL, 'Germain', 9876, 2),
(2, 12, 'germain.ik@gmail.com', 'Xasdfsdfsmiver', NULL, 'Gesdfsdrmain', 9876, 2),
(3, 762127, 'henry.ik@gmail.com', 'Konan', NULL, 'Henry', 789987, 2),
(4, 767, 'isaac07.ik@gmail.com', 'Kodjo', NULL, 'Isaac', 77886677, 1);

-- --------------------------------------------------------

--
-- Table structure for table `criteres`
--

CREATE TABLE `criteres` (
  `criteres_id` int(11) NOT NULL,
  `candidat_bareme` double DEFAULT NULL,
  `criteres_libelle` varchar(255) DEFAULT NULL,
  `evenement_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evenement`
--

CREATE TABLE `evenement` (
  `id` int(11) NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evenement`
--

INSERT INTO `evenement` (`id`, `date_debut`, `date_fin`, `nom`, `type`) VALUES
(3, '2021-01-19 17:05:53', '2021-01-21 17:05:53', 'iwwwwwwwwwsadac', 'kodjo'),
(4, '2021-01-15 17:05:53', '2021-01-17 17:05:53', 'oci', 'orange'),
(5, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(6, '2021-01-19 17:05:53', '2021-01-21 17:05:53', 'Ahiba', 'matrix'),
(7, '2021-01-19 17:05:53', '2021-01-21 17:05:53', 'WWWgfgdfgWWW', 'PPPPPP'),
(8, '2021-01-19 17:05:53', '2021-01-21 17:05:53', 'Ahifdddddba', 'matriddddddx'),
(9, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(10, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(11, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(12, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(13, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(14, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(15, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(16, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadac', 'kodjo'),
(17, '2021-02-19 17:05:53', '2021-01-23 17:05:53', 'isadsdfsac', 'kosdfsdjo');

-- --------------------------------------------------------

--
-- Table structure for table `groupes`
--

CREATE TABLE `groupes` (
  `groupe_id` int(11) NOT NULL,
  `evenement_id` int(11) NOT NULL,
  `groupe_code` int(11) NOT NULL,
  `groupe_nom` varchar(255) DEFAULT NULL,
  `groupe_photo` tinyblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groupes_candidats`
--

CREATE TABLE `groupes_candidats` (
  `groupe_candidat_id` int(11) NOT NULL,
  `candidat_id` int(11) NOT NULL,
  `groupe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `jury`
--

CREATE TABLE `jury` (
  `jury_id` int(11) NOT NULL,
  `code_id` int(11) NOT NULL,
  `evenement_id` int(11) NOT NULL,
  `jury_email` varchar(255) DEFAULT NULL,
  `jury_nom_complet` varchar(255) DEFAULT NULL,
  `jury_telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vote_candidats`
--

CREATE TABLE `vote_candidats` (
  `vote_candidat_id` int(11) NOT NULL,
  `candidat_id` int(11) NOT NULL,
  `commentaires` varchar(255) DEFAULT NULL,
  `evenement_id` int(11) NOT NULL,
  `jury_id` int(11) NOT NULL,
  `note` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vote_groupes`
--

CREATE TABLE `vote_groupes` (
  `vote_groupe_id` int(11) NOT NULL,
  `candidat_id` int(11) NOT NULL,
  `commentaires` varchar(255) DEFAULT NULL,
  `criteres_id` int(11) NOT NULL,
  `evenement_id` int(11) NOT NULL,
  `jury_id` int(11) NOT NULL,
  `note` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidats`
--
ALTER TABLE `candidats`
  ADD PRIMARY KEY (`candidat_id`);

--
-- Indexes for table `criteres`
--
ALTER TABLE `criteres`
  ADD PRIMARY KEY (`criteres_id`);

--
-- Indexes for table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groupes`
--
ALTER TABLE `groupes`
  ADD PRIMARY KEY (`groupe_id`);

--
-- Indexes for table `groupes_candidats`
--
ALTER TABLE `groupes_candidats`
  ADD PRIMARY KEY (`groupe_candidat_id`);

--
-- Indexes for table `jury`
--
ALTER TABLE `jury`
  ADD PRIMARY KEY (`jury_id`);

--
-- Indexes for table `vote_candidats`
--
ALTER TABLE `vote_candidats`
  ADD PRIMARY KEY (`vote_candidat_id`);

--
-- Indexes for table `vote_groupes`
--
ALTER TABLE `vote_groupes`
  ADD PRIMARY KEY (`vote_groupe_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidats`
--
ALTER TABLE `candidats`
  MODIFY `candidat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `criteres`
--
ALTER TABLE `criteres`
  MODIFY `criteres_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `groupes`
--
ALTER TABLE `groupes`
  MODIFY `groupe_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groupes_candidats`
--
ALTER TABLE `groupes_candidats`
  MODIFY `groupe_candidat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jury`
--
ALTER TABLE `jury`
  MODIFY `jury_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_candidats`
--
ALTER TABLE `vote_candidats`
  MODIFY `vote_candidat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_groupes`
--
ALTER TABLE `vote_groupes`
  MODIFY `vote_groupe_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Database: `kasbon`
--
CREATE DATABASE IF NOT EXISTS `kasbon` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `kasbon`;
--
-- Database: `lara_jwt`
--
CREATE DATABASE IF NOT EXISTS `lara_jwt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lara_jwt`;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John', 'John@test.com', NULL, '$2y$10$aqkb3QrpvrBo6tCKe1MEWOl96eTFrxGF.s/EzDH1Fkyynod0ylgz.', NULL, '2020-07-15 13:52:31', '2020-07-15 13:52:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Database: `lumenapidb`
--
CREATE DATABASE IF NOT EXISTS `lumenapidb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lumenapidb`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `lumen_jwt`
--
CREATE DATABASE IF NOT EXISTS `lumen_jwt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lumen_jwt`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2020_06_26_122340_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Clemmie Langworth III', 'manley.wunsch@yahoo.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(2, 'Dr. Raven Russel', 'maybell71@barrows.org', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(3, 'Ariel Beatty', 'pagac.joelle@bosco.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(4, 'Stella Reilly', 'qkeeling@hotmail.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(5, 'Yadira Treutel', 'katrina.jacobs@gmail.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(6, 'Miss Hosea Turner Jr.', 'nora66@yahoo.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(7, 'Prof. Howard Greenfelder III', 'corwin.jessy@wisozk.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(8, 'Dr. Jeramy Powlowski PhD', 'shayna.jaskolski@lesch.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(9, 'Miss Delia Carroll II', 'friesen.jerome@gmail.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01'),
(10, 'Mr. Samir Stiedemann III', 'jones.amani@kutch.com', '2020-06-26 12:35:01', '2020-06-26 12:35:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Database: `mvc`
--
CREATE DATABASE IF NOT EXISTS `mvc` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mvc`;
--
-- Database: `myblog`
--
CREATE DATABASE IF NOT EXISTS `myblog` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `myblog`;
--
-- Database: `mydb`
--
CREATE DATABASE IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mydb`;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`) VALUES
(2, 'XBox', 399.88),
(3, 'XBox 360', 299.99);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Database: `myhobbies_english`
--
CREATE DATABASE IF NOT EXISTS `myhobbies_english` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `myhobbies_english`;
--
-- Database: `oda`
--
CREATE DATABASE IF NOT EXISTS `oda` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `oda`;

-- --------------------------------------------------------

--
-- Table structure for table `academiciens`
--

CREATE TABLE `academiciens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fonction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `academiciens`
--

INSERT INTO `academiciens` (`id`, `nom`, `fonction`, `direction`, `site`, `created_at`, `updated_at`) VALUES
(1, 'TA Bi Stéphane', 'Stagiaire', 'DTDM', 'Orange Academy', NULL, NULL),
(2, 'Sylla Hamed', 'Stagiaire', 'DTDM', 'Orange Academy', '2020-07-06 15:18:57', '2020-07-06 15:18:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_07_06_142138_creata_academiciens_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academiciens`
--
ALTER TABLE `academiciens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academiciens`
--
ALTER TABLE `academiciens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Database: `parc_auto`
--
CREATE DATABASE IF NOT EXISTS `parc_auto` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `parc_auto`;

-- --------------------------------------------------------

--
-- Table structure for table `achats`
--

CREATE TABLE `achats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `vehicule_id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `prixht` double(8,2) NOT NULL,
  `prixttc` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenoms` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_10_07_180356_create_clients_table', 1),
(2, '2020_10_07_180449_create_vehicules_table', 1),
(3, '2020_10_08_101746_create_achats_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicules`
--

CREATE TABLE `vehicules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modele` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `annee` date NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achats`
--
ALTER TABLE `achats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicules`
--
ALTER TABLE `vehicules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achats`
--
ALTER TABLE `achats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vehicules`
--
ALTER TABLE `vehicules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `peoplefinder`
--
CREATE DATABASE IF NOT EXISTS `peoplefinder` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `peoplefinder`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_07_08_112402_create_people_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `first_name`, `last_name`, `phone`, `email`, `city`, `created_at`, `updated_at`) VALUES
(1, 'Kacey', 'Lueilwitz', '258-795-4474 x1544', 'vicenta53@example.net', 'New Kelvin', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(2, 'Bradford', 'Grady', '1-663-778-2395', 'igibson@example.net', 'North Janetberg', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(3, 'Annamarie', 'Schultz', '934.640.6085', 'makenna.bechtelar@example.com', 'New Summer', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(4, 'Lillie', 'Denesik', '740.215.5970 x876', 'krystina55@example.net', 'East Devyn', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(5, 'Henri', 'Gaylord', '1-752-842-1795 x94715', 'kellen44@example.org', 'East Lestertown', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(6, 'Elinor', 'Kirlin', '763.428.9957 x14725', 'astrid09@example.net', 'Carahaven', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(7, 'Marlon', 'Herzog', '(312) 228-0821 x769', 'stanton.kacey@example.com', 'Port Genovevastad', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(8, 'Matilda', 'Hettinger', '+1.823.986.6433', 'nya.walter@example.net', 'Rohanbury', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(9, 'Kaci', 'Beer', '534-849-4061 x4139', 'dale24@example.net', 'Lake Anabelhaven', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(10, 'Clifford', 'Howe', '1-456-267-2081 x216', 'uoconnell@example.net', 'New Rosalia', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(11, 'Newell', 'Jast', '750-354-8380', 'catherine.gorczany@example.com', 'Manteville', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(12, 'Dejon', 'Blick', '(921) 552-1710 x30653', 'alison50@example.org', 'Lake Mattchester', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(13, 'Johan', 'Stracke', '919-387-4350 x38537', 'kdietrich@example.org', 'Port Libbiechester', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(14, 'Eliza', 'Klocko', '664.793.3893', 'nasir85@example.com', 'North Vernie', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(15, 'Nikko', 'Fadel', '(632) 905-7986', 'jordane60@example.com', 'New Annettaside', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(16, 'Lavina', 'Kozey', '+1.916.616.6545', 'dare.madyson@example.org', 'West Alfburgh', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(17, 'Albin', 'Kunze', '242-414-0001 x10009', 'jalen26@example.org', 'Jadonstad', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(18, 'Michel', 'Thiel', '861-782-4635', 'opadberg@example.net', 'Kuhnshire', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(19, 'Leon', 'Hermiston', '+1-592-901-3281', 'feeney.lamar@example.com', 'West Tobinmouth', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(20, 'Jeffery', 'O\'Connell', '+13506164564', 'larkin.erika@example.com', 'East Gus', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(21, 'Earl', 'Labadie', '660-453-9552', 'huel.xavier@example.org', 'Elijahland', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(22, 'Eileen', 'Schimmel', '1-201-412-1602', 'cwindler@example.net', 'Catalinachester', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(23, 'Eda', 'Lockman', '+1.718.862.3393', 'stokes.jeromy@example.com', 'Port Einotown', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(24, 'Candelario', 'Haag', '+1-935-366-1444', 'sanford.wilber@example.net', 'New Reynoldhaven', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(25, 'Brooklyn', 'Konopelski', '295.278.7006', 'brant03@example.com', 'South Orlandshire', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(26, 'Ruth', 'Schultz', '+1-878-579-1708', 'linnea52@example.org', 'Port Aileenmouth', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(27, 'Alysson', 'Klein', '392-917-4362', 'lynch.brendan@example.net', 'Lake Leopold', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(28, 'Kamille', 'Hoppe', '(457) 413-5411 x6955', 'kmuller@example.org', 'New Bessie', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(29, 'Cyrus', 'Botsford', '705-522-7210 x9567', 'borer.fidel@example.org', 'Claudechester', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(30, 'Stone', 'Kshlerin', '648-714-5139', 'qtowne@example.net', 'Francomouth', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(31, 'Irving', 'Conn', '(509) 716-1203 x13004', 'urodriguez@example.com', 'Lake Wiltonview', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(32, 'Reymundo', 'Watsica', '1-958-989-8667 x75701', 'austen.johnston@example.org', 'Parisville', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(33, 'Della', 'Grant', '+1.284.734.1913', 'loyce82@example.net', 'Marilynebury', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(34, 'Edmund', 'Murray', '+1-203-645-0166', 'halie.crona@example.org', 'East Adrainland', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(35, 'Dulce', 'Dare', '+13478748374', 'fritsch.issac@example.net', 'East Orrin', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(36, 'Mara', 'Gutmann', '1-506-643-7205 x985', 'hamill.alva@example.org', 'North Kim', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(37, 'Daron', 'Emard', '1-814-536-0970', 'dgottlieb@example.net', 'Port Oriefurt', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(38, 'Luna', 'Rohan', '+17653035897', 'parker.stroman@example.org', 'Nataliebury', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(39, 'Sydni', 'Hermann', '1-936-802-3244', 'jaskolski.orion@example.org', 'Deborahchester', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(40, 'Charity', 'Rath', '+1 (770) 261-6609', 'willa.powlowski@example.com', 'West Mona', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(41, 'Lorena', 'Gutmann', '1-430-702-3055', 'rhagenes@example.net', 'South Angelita', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(42, 'Carolyne', 'Langworth', '390-378-9270', 'renner.dane@example.net', 'Brendanstad', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(43, 'Demarcus', 'Cremin', '489-629-2418 x8485', 'ricky.hoppe@example.com', 'Greenholtfort', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(44, 'Lexi', 'Beier', '1-932-317-2359 x142', 'qjenkins@example.net', 'Brookeland', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(45, 'River', 'VonRueden', '1-786-705-3524 x7086', 'frederique80@example.net', 'South Sonya', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(46, 'Margot', 'Grimes', '(910) 762-7663 x571', 'mohammad.gislason@example.net', 'Rocioport', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(47, 'Elbert', 'Kunde', '(883) 515-6255 x09882', 'darien.rosenbaum@example.net', 'South Donnyborough', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(48, 'Karli', 'Denesik', '720.202.1047', 'savanah.abshire@example.net', 'Gusikowskitown', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(49, 'Colt', 'Zemlak', '(619) 509-2746 x339', 'lea.dooley@example.net', 'Larkinshire', '2020-07-08 11:40:48', '2020-07-08 11:40:48'),
(50, 'Jaden', 'Johnson', '(670) 909-1482 x07669', 'andreane.metz@example.com', 'Claudieborough', '2020-07-08 11:40:48', '2020-07-08 11:40:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- Database: `restaurantoda`
--
CREATE DATABASE IF NOT EXISTS `restaurantoda` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `restaurantoda`;

-- --------------------------------------------------------

--
-- Table structure for table `categ_plats`
--

CREATE TABLE `categ_plats` (
  `IDCATEG` int(11) NOT NULL,
  `LIBCATEG` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `commandes`
--

CREATE TABLE `commandes` (
  `IDCMD` int(11) NOT NULL,
  `IDUSER` int(11) NOT NULL,
  `DTECMD` date DEFAULT NULL,
  `FRAIS` int(11) DEFAULT NULL,
  `REDUCTION` int(11) DEFAULT NULL,
  `STATUT` int(11) DEFAULT NULL,
  `VALIDE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contenir`
--

CREATE TABLE `contenir` (
  `IDCMD` int(11) NOT NULL,
  `IDPLAT` int(11) NOT NULL,
  `QTECMD` int(11) DEFAULT NULL,
  `PRIXCMD` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `etre`
--

CREATE TABLE `etre` (
  `IDPLAT` int(11) NOT NULL,
  `IDMENU` int(11) NOT NULL,
  `QTE` int(11) DEFAULT NULL,
  `PRIX` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `IDMENU` int(11) NOT NULL,
  `DTEDEBUT` date DEFAULT NULL,
  `DTEFIN` date DEFAULT NULL,
  `DESCRIPTION` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paiements`
--

CREATE TABLE `paiements` (
  `IDPAIEMENT` int(11) NOT NULL,
  `IDCMD` int(11) NOT NULL,
  `DTEPAIEMENT` date DEFAULT NULL,
  `PRIX` float DEFAULT NULL,
  `TYPEPAIEMENT` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plats`
--

CREATE TABLE `plats` (
  `IDPLAT` int(11) NOT NULL,
  `IDTYPEPLAT` int(11) NOT NULL,
  `IDCATEG` int(11) NOT NULL,
  `IDUSER` int(11) NOT NULL,
  `LIBPLAT` varchar(160) DEFAULT NULL,
  `DESCPLAT` varchar(160) DEFAULT NULL,
  `IMG` varchar(255) DEFAULT NULL,
  `PRIX` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `IDROLE` int(11) NOT NULL,
  `LIBROLE` varchar(160) DEFAULT NULL,
  `DESCROLE` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `type_plats`
--

CREATE TABLE `type_plats` (
  `IDTYPEPLAT` int(11) NOT NULL,
  `LIBTYPEPLAT` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `IDUSER` int(11) NOT NULL,
  `IDROLE` int(11) NOT NULL,
  `NOM` varchar(160) DEFAULT NULL,
  `PNOM` varchar(160) DEFAULT NULL,
  `DTNAISS` date DEFAULT NULL,
  `CONTACT` varchar(50) DEFAULT NULL,
  `COMMUNE` varchar(50) DEFAULT NULL,
  `LOGIN` varchar(20) DEFAULT NULL,
  `MDP` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categ_plats`
--
ALTER TABLE `categ_plats`
  ADD PRIMARY KEY (`IDCATEG`);

--
-- Indexes for table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`IDCMD`),
  ADD KEY `FK_PASSER` (`IDUSER`);

--
-- Indexes for table `contenir`
--
ALTER TABLE `contenir`
  ADD PRIMARY KEY (`IDCMD`,`IDPLAT`),
  ADD KEY `FK_CONTENIR2` (`IDPLAT`);

--
-- Indexes for table `etre`
--
ALTER TABLE `etre`
  ADD PRIMARY KEY (`IDPLAT`,`IDMENU`),
  ADD KEY `FK_ETRE2` (`IDMENU`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`IDMENU`);

--
-- Indexes for table `paiements`
--
ALTER TABLE `paiements`
  ADD PRIMARY KEY (`IDPAIEMENT`),
  ADD KEY `FK_PAYER` (`IDCMD`);

--
-- Indexes for table `plats`
--
ALTER TABLE `plats`
  ADD PRIMARY KEY (`IDPLAT`),
  ADD KEY `FK_AJOUTER` (`IDUSER`),
  ADD KEY `FK_CONCERNER` (`IDTYPEPLAT`),
  ADD KEY `FK_LIER` (`IDCATEG`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`IDROLE`);

--
-- Indexes for table `type_plats`
--
ALTER TABLE `type_plats`
  ADD PRIMARY KEY (`IDTYPEPLAT`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`IDUSER`),
  ADD KEY `FK_AVOIR` (`IDROLE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categ_plats`
--
ALTER TABLE `categ_plats`
  MODIFY `IDCATEG` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `IDCMD` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `IDMENU` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paiements`
--
ALTER TABLE `paiements`
  MODIFY `IDPAIEMENT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plats`
--
ALTER TABLE `plats`
  MODIFY `IDPLAT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `IDROLE` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_plats`
--
ALTER TABLE `type_plats`
  MODIFY `IDTYPEPLAT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `IDUSER` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `FK_PASSER` FOREIGN KEY (`IDUSER`) REFERENCES `users` (`IDUSER`);

--
-- Constraints for table `contenir`
--
ALTER TABLE `contenir`
  ADD CONSTRAINT `FK_CONTENIR` FOREIGN KEY (`IDCMD`) REFERENCES `commandes` (`IDCMD`),
  ADD CONSTRAINT `FK_CONTENIR2` FOREIGN KEY (`IDPLAT`) REFERENCES `plats` (`IDPLAT`);

--
-- Constraints for table `etre`
--
ALTER TABLE `etre`
  ADD CONSTRAINT `FK_ETRE` FOREIGN KEY (`IDPLAT`) REFERENCES `plats` (`IDPLAT`),
  ADD CONSTRAINT `FK_ETRE2` FOREIGN KEY (`IDMENU`) REFERENCES `menus` (`IDMENU`);

--
-- Constraints for table `paiements`
--
ALTER TABLE `paiements`
  ADD CONSTRAINT `FK_PAYER` FOREIGN KEY (`IDCMD`) REFERENCES `commandes` (`IDCMD`);

--
-- Constraints for table `plats`
--
ALTER TABLE `plats`
  ADD CONSTRAINT `FK_AJOUTER` FOREIGN KEY (`IDUSER`) REFERENCES `users` (`IDUSER`),
  ADD CONSTRAINT `FK_CONCERNER` FOREIGN KEY (`IDTYPEPLAT`) REFERENCES `type_plats` (`IDTYPEPLAT`),
  ADD CONSTRAINT `FK_LIER` FOREIGN KEY (`IDCATEG`) REFERENCES `categ_plats` (`IDCATEG`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_AVOIR` FOREIGN KEY (`IDROLE`) REFERENCES `roles` (`IDROLE`);
--
-- Database: `school`
--
CREATE DATABASE IF NOT EXISTS `school` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `school`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2020_06_26_160226_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Georgiana Strosin', 'vgraham@dare.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(2, 'Malika Lesch', 'lang.jude@gmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(3, 'Miss Eva Hamill DDS', 'jaylan03@hotmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(4, 'Hilton Kuphal', 'bartoletti.leonor@yahoo.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(5, 'Mrs. Zaria Reichert Sr.', 'jess.collier@hotmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(6, 'Prof. Shaylee Torp DDS', 'rreichel@gmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(7, 'Dr. Jennifer Metz V', 'sven.krajcik@stiedemann.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(8, 'Fanny Durgan', 'jhalvorson@fay.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(9, 'Mr. Royal Satterfield DDS', 'schmitt.noemy@harber.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(10, 'River Kautzer', 'wintheiser.rosalinda@hotmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(11, 'Bethany Reynolds PhD', 'cward@gmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(12, 'Layne Jacobson', 'marlon.tromp@boyle.biz', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(13, 'Arlo Rodriguez', 'delia.moen@rogahn.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(14, 'Dr. Rosetta Schaden', 'flo.reynolds@beatty.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(15, 'Marion Wiza', 'stoltenberg.ayden@hotmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(16, 'Dr. Devante Anderson', 'tressie.mann@bartoletti.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(17, 'Mauricio Moore', 'conroy.jamir@roob.biz', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(18, 'Genesis Tillman', 'david.wilkinson@hotmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(19, 'Alvena Nikolaus', 'yschneider@gmail.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40'),
(20, 'Elisha Hahn I', 'asha09@waters.com', '2020-06-26 16:20:40', '2020-06-26 16:20:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Database: `smartTrash`
--
CREATE DATABASE IF NOT EXISTS `smartTrash` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `smartTrash`;

-- --------------------------------------------------------

--
-- Table structure for table `activites`
--

CREATE TABLE `activites` (
  `idactivite` int(10) UNSIGNED NOT NULL,
  `idequipement` int(10) UNSIGNED NOT NULL,
  `niveau` double(8,2) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `concernes`
--

CREATE TABLE `concernes` (
  `idconcerne` int(10) UNSIGNED NOT NULL,
  `idpoubelle` int(10) UNSIGNED NOT NULL,
  `idmission` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `equipements`
--

CREATE TABLE `equipements` (
  `idequipement` int(10) UNSIGNED NOT NULL,
  `numequipement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equipements`
--

INSERT INTO `equipements` (`idequipement`, `numequipement`, `created_at`, `updated_at`) VALUES
(1, 'arduino123', '2020-07-03 11:11:02', '2020-07-03 11:11:02'),
(2, 'arduino124', '2020-07-03 11:12:56', '2020-07-03 11:12:56'),
(3, 'arduino125', '2020-07-03 11:13:03', '2020-07-03 11:13:03'),
(4, 'arduino126', '2020-07-03 11:13:08', '2020-07-03 11:13:08'),
(5, 'arduino127', '2020-07-03 11:13:14', '2020-07-03 11:13:14'),
(6, 'arduino128', '2020-07-03 11:13:19', '2020-07-03 11:13:19'),
(7, 'arduino129', '2020-07-03 11:13:25', '2020-07-03 11:13:25'),
(8, 'arduino133', '2020-07-03 11:13:52', '2020-07-03 11:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `installes`
--

CREATE TABLE `installes` (
  `idinstalle` int(10) UNSIGNED NOT NULL,
  `idequipement` int(10) UNSIGNED NOT NULL,
  `idpoubelle` int(10) UNSIGNED NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_06_29_110533_create_equipements_table', 1),
(2, '2020_06_29_111303_create_roles_table', 1),
(3, '2020_06_29_111528_create_zones_table', 1),
(4, '2020_06_29_111703_create_vehicules_table', 1),
(5, '2020_06_29_112033_create_utilisateurs_table', 1),
(6, '2020_06_29_114017_create_missions_table', 1),
(7, '2020_06_29_114742_create_poubelles_table', 1),
(8, '2020_06_29_115214_create_activites_table', 1),
(9, '2020_06_29_115836_create_travailles_table', 1),
(10, '2020_06_29_120323_create_concernes_table', 1),
(11, '2020_06_29_120546_create_installes_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `missions`
--

CREATE TABLE `missions` (
  `idmission` int(10) UNSIGNED NOT NULL,
  `idvehicule` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateExecution` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `missions`
--

INSERT INTO `missions` (`idmission`, `idvehicule`, `libelle`, `dateExecution`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Yopougon', '2020-07-15', 'ramassage des ordures', '2020-07-15 10:31:16', '2020-07-15 10:31:16'),
(2, 28, 'Marcory', '2020-07-16', 'Ramassage des ordures', '2020-07-16 10:37:00', '2020-07-16 10:37:00'),
(3, 4, 'Treichville', '2020-07-16', 'ramassage des ordures', '2020-07-16 10:37:57', '2020-07-16 10:37:57'),
(4, 6, 'Abobo', '2020-07-14', 'ramassage des ordures', '2020-07-14 10:38:44', '2020-07-14 10:38:44'),
(5, 29, 'Plateau', '2020-07-13', 'ramassage des ordures', '2020-07-13 10:39:33', '2020-07-13 10:39:33');

-- --------------------------------------------------------

--
-- Table structure for table `poubelles`
--

CREATE TABLE `poubelles` (
  `idpoubelle` int(10) UNSIGNED NOT NULL,
  `idzone` int(10) UNSIGNED NOT NULL,
  `numeroPoubelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `poubelles`
--

INSERT INTO `poubelles` (`idpoubelle`, `idzone`, `numeroPoubelle`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, '123Poub', 'Azerty', '2020-07-03 10:53:43', '2020-07-03 10:53:43'),
(3, 1, '124Poub', 'Qwerty', '2020-07-03 10:54:37', '2020-07-03 14:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `idrole` int(10) UNSIGNED NOT NULL,
  `libellerole` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`idrole`, `libellerole`, `created_at`, `updated_at`) VALUES
(1, 'Agent', '2020-07-01 15:26:41', '2020-07-01 15:26:41'),
(2, 'Superviseur', '2020-07-01 15:26:41', '2020-07-01 15:26:41');

-- --------------------------------------------------------

--
-- Table structure for table `travailles`
--

CREATE TABLE `travailles` (
  `idtravail` int(10) UNSIGNED NOT NULL,
  `iduser` int(10) UNSIGNED NOT NULL,
  `idmission` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `travailles`
--

INSERT INTO `travailles` (`idtravail`, `iduser`, `idmission`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '2020-07-15', '2020-07-15 11:42:51', '2020-07-15 11:42:51'),
(2, 1, 2, '2020-07-16', '2020-07-16 11:43:18', '2020-07-16 11:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `iduser` int(10) UNSIGNED NOT NULL,
  `idrole` int(10) UNSIGNED NOT NULL,
  `matricule` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datenaiss` date NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mdp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lieuResidence` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `utilisateurs`
--

INSERT INTO `utilisateurs` (`iduser`, `idrole`, `matricule`, `nom`, `prenom`, `datenaiss`, `login`, `mdp`, `contact`, `lieuResidence`, `created_at`, `updated_at`) VALUES
(1, 1, 'MAT005', 'Touré', 'Gaoussou', '1989-11-06', 'MAT005', 'MAT005', '09080706', 'Cocody', '2020-07-01 15:27:46', '2020-07-01 15:27:46'),
(2, 2, 'MAT007', 'Ta', 'Bi STéphane', '1800-07-16', 'opentecno', 'opentecno', '09080706', 'Yopougon', '2020-07-01 15:27:46', '2020-07-01 15:27:46'),
(3, 1, 'MAT698', 'Mayer', 'Felton', '1989-01-11', 'kenyatta10', '4}5?Owo\'$h,V`7&t', '1-394-541-6063 x953', '3735 Geraldine Ford\nSpencerstad, AK 44299', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(4, 1, 'MAT907', 'Lindgren', 'Newell', '2020-06-17', 'major.kunze', 'D.NU>4zhL', '704.381.8575 x1469', '199 Selina Ford\nLake Vella, NJ 16047', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(5, 1, 'MAT960', 'Osinski', 'Jett', '1977-07-04', 'verda35', 's].rEm.)-SA', '+1.371.719.5399', '88835 Dejah Square Apt. 597\nLake Wilbert, MA 49197-8437', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(6, 1, 'MAT467', 'Schaefer', 'Arno', '1988-04-17', 'glang', 'n^i1{e7+gt$', '1-859-569-7589', '9411 Lorenz Meadow Apt. 577\nGusikowskibury, AK 16910', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(7, 1, 'MAT291', 'Sauer', 'Mason', '2003-08-06', 'ophelia.homenick', '!+*3}g|[N|9}PD>bj)X', '+13695273209', '90013 Kovacek Harbors\nSouth Lizethtown, OK 26390', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(8, 1, 'MAT460', 'Pollich', 'Grant', '2012-11-01', 'jaskolski.adella', '3r_`{w', '1-545-625-7671', '206 Urban Points\nPort Abe, NV 92042-4926', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(9, 1, 'MAT635', 'Abshire', 'Dedrick', '1987-01-20', 'gustave64', 'v:2XGu<>', '(767) 245-3551 x19998', '61077 Schuster Road\nLake Demetris, CO 44114', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(10, 1, 'MAT808', 'Bins', 'Avery', '2016-05-17', 'uroob', ';.q\\]E\"k31', '1-270-499-9664 x8758', '799 Swift Island\nNew Alexandria, OR 09551-1493', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(11, 1, 'MAT867', 'Baumbach', 'Geoffrey', '2001-09-12', 'okon.gertrude', 'p7Z^GgZNb}/Ofw', '+12714541158', '2822 Lockman Skyway Suite 521\nMoenborough, HI 45256', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(12, 1, 'MAT285', 'Russel', 'Trevion', '1991-10-27', 'mcclure.lucy', '8MD+RbZ6MiI-:i', '1-572-509-7701 x1361', '120 Jast Shore Apt. 764\nLake Jaquanville, DC 15159-1285', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(13, 1, 'MAT183', 'Pollich', 'Lula', '1978-04-22', 'larry59', '/4IS~&\'N!`7lxkQd~8`', '621.225.1586', '65050 Stoltenberg Meadows\nWest Dion, WY 83111', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(14, 1, 'MAT214', 'Ankunding', 'Hermann', '2018-08-02', 'mhane', '`ZUd]\\37HfX/', '1-619-245-1143', '783 Connie Views\nEast Ambroseview, NC 09435-1773', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(15, 1, 'MAT494', 'McCullough', 'Evert', '2005-03-24', 'emmerich.jeramy', '\'S[go5WiUi3#@', '+1 (950) 384-4117', '6635 Ruecker Highway Suite 658\nRashadshire, SD 54741', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(16, 1, 'MAT988', 'Oberbrunner', 'Clifton', '1998-05-07', 'olarkin', 'Ut;\"<RVb3!OyUyYp', '(663) 500-6241', '916 DuBuque Motorway Apt. 455\nLake Cleveburgh, FL 67741-4240', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(17, 1, 'MAT086', 'Jast', 'Isaias', '1988-12-14', 'erdman.frank', '&t\\yB55jjGgt', '1-454-847-3899', '550 Prohaska Inlet Apt. 474\nIsabellamouth, VA 48795-1417', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(18, 1, 'MAT192', 'Kohler', 'Jayson', '2000-08-25', 'wiza.tomas', 'OHTO.GDfmLN%W', '+1 (515) 282-2375', '8998 Yost Key\nEast Amelyport, KS 85644', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(19, 1, 'MAT550', 'Miller', 'Bernard', '1991-09-07', 'ykautzer', 'e;ps>+ClZZSku\\K', '(669) 658-9969', '2037 Blick Springs\nSouth Cadenberg, OH 21954', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(20, 1, 'MAT644', 'Baumbach', 'Adriel', '1992-09-17', 'meggie46', 'G|bp\"\'', '+18383758253', '3475 Kris Well Apt. 714\nSouth Valentinville, SC 39678-1858', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(21, 1, 'MAT530', 'Erdman', 'Kayden', '1986-12-11', 'bartholome52', '9(^$4fDGi(\\U}-S_W', '+1.716.685.7928', '2173 Jaden Springs Suite 769\nAbbiefurt, GA 45072-3798', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(22, 1, 'MAT449', 'Harris', 'Rosendo', '1985-11-07', 'albin35', 'r2l^hueDrf{N_', '1-336-451-3316', '91204 Nikolaus Glen Apt. 097\nNorth Clint, MD 28790', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(23, 1, 'MAT487', 'Stehr', 'Thomas', '1989-10-29', 'jklein', 'mj]g<!@7[X', '(641) 343-4131', '3490 Bo Lodge Apt. 317\nHarveyburgh, VA 12550-1610', '2020-07-02 11:58:25', '2020-07-02 11:58:25'),
(24, 1, 'MAT025', 'Hane', 'Kennedy', '2007-05-19', 'kaelyn.dibbert', ']GM;^up5&YLz', '(332) 462-1574 x8195', '5508 Leora Fall\nDannybury, AL 55415-0900', '2020-07-02 11:58:25', '2020-07-02 11:58:25');

-- --------------------------------------------------------

--
-- Table structure for table `vehicules`
--

CREATE TABLE `vehicules` (
  `idvehicule` int(10) UNSIGNED NOT NULL,
  `immatriculation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modele` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `couleur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicules`
--

INSERT INTO `vehicules` (`idvehicule`, `immatriculation`, `modele`, `marque`, `couleur`, `created_at`, `updated_at`) VALUES
(1, 'ANZ-711', 'SHK', 'Wanfeng', 'Pink', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(2, 'JQZ-955', 'Arcadia', 'Daewoo', 'Darkorange', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(3, 'IOX-227', '650S', 'McLaren', 'Thistle', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(4, 'GOG-000', 'DG-C2', 'Marshell', 'DimGray', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(5, 'KQR-177', '6371 пасс.', 'FAW', 'RoyalBlue', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(6, 'CYT-785', '350Z', 'Nissan', 'LawnGreen', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(7, 'LMR-391', 'Yugo', 'Zastava', 'MediumAquaMarine', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(8, 'QGX-044', '21', 'Caterham', 'Cornsilk', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(9, 'MYE-777', 'S7', 'Saleen', 'RosyBrown', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(10, 'WQQ-191', 'Club', 'Oltcit', 'DarkOrchid', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(11, 'GBK-522', 'Galaxie', 'Ford', 'Peru', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(12, 'TDI-470', 'Elba', 'Innocenti', 'LightSalmon', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(13, 'UBJ-730', 'Sahin', 'Tofas', 'Wheat', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(14, 'FGA-459', 'GT', 'Artega', 'RoyalBlue', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(15, 'HXP-130', '7', 'Caterham', 'LightPink', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(16, 'LHQ-555', 'MKZ', 'Lincoln', 'SandyBrown', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(17, 'RNQ-153', 'Chairman', 'SsangYong', 'HotPink', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(18, 'EBL-243', 'Freeclimber', 'Bertone', 'SlateBlue', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(19, 'CON-720', 'Zonda', 'Pagani', 'DarkBlue', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(20, 'DTF-272', 'Maxus', 'LDV', 'PeachPuff', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(21, 'HQU-478', 'Fortwo', 'Smart', 'BurlyWood', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(22, 'FNH-971', '30', 'Aero', 'Coral', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(23, 'UMW-053', '640', 'BMW', 'Silver', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(24, 'NVM-320', 'Prizm', 'Geo', 'MediumSeaGreen', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(25, 'PHA-907', 'DN', 'Marshell', 'MediumVioletRed', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(26, 'VAO-600', 'ML 350', 'Mercedes-Benz', 'DarkSlateGray', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(27, 'PHA-344', 'TAXI', 'Samand', 'LightGray', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(28, 'BOB-564', 'Monterey', 'Vauxhall', 'LightSkyBlue', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(29, 'CBA-253', 'IS 220', 'Lexus', 'DarkSlateGray', '2020-07-02 11:59:33', '2020-07-02 11:59:33'),
(30, 'VXQ-891', '427 KMS', 'Kirkham', 'Gainsboro', '2020-07-02 11:59:33', '2020-07-02 11:59:33');

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `idzone` int(10) UNSIGNED NOT NULL,
  `libellezone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`idzone`, `libellezone`, `created_at`, `updated_at`) VALUES
(1, 'Lottieshire', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(2, 'yopougon', '2020-07-02 11:57:25', '2020-07-03 10:13:46'),
(3, 'marcory', '2020-07-02 11:57:25', '2020-07-03 10:56:13'),
(4, 'East', '2020-07-02 11:57:25', '2020-07-03 10:26:05'),
(5, 'Olsonton', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(6, 'West Frederiqueton', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(7, 'Brendenland', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(8, 'Koepptown', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(9, 'East Xavierborough', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(10, 'Port Maximusmouth', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(11, 'Port Marcia', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(12, 'Earlview', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(13, 'South Brad', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(14, 'Keeblerville', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(15, 'New Amanda', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(16, 'Port Blancastad', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(17, 'West Elvie', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(18, 'Towneside', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(19, 'North Ressiefort', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(20, 'Blickberg', '2020-07-02 11:57:25', '2020-07-02 11:57:25'),
(21, 'Yopougon', '2020-07-02 13:46:29', '2020-07-02 13:46:29'),
(22, 'Adjame', '2020-07-02 13:47:01', '2020-07-02 13:47:01'),
(23, 'Treichville', '2020-07-02 13:50:23', '2020-07-02 13:50:23'),
(24, 'Cocody', '2020-07-02 14:05:23', '2020-07-02 14:05:23'),
(25, 'Marcory', '2020-07-02 14:06:06', '2020-07-02 14:06:06'),
(26, 'Bassam', '2020-07-02 14:07:12', '2020-07-02 14:07:12'),
(27, 'Bassam', '2020-07-02 14:08:26', '2020-07-02 14:08:26'),
(28, 'bingerville', '2020-07-03 10:56:55', '2020-07-03 10:56:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activites`
--
ALTER TABLE `activites`
  ADD PRIMARY KEY (`idactivite`),
  ADD KEY `activites_idequipement_foreign` (`idequipement`);

--
-- Indexes for table `concernes`
--
ALTER TABLE `concernes`
  ADD PRIMARY KEY (`idconcerne`),
  ADD KEY `concernes_idpoubelle_foreign` (`idpoubelle`),
  ADD KEY `concernes_idmission_foreign` (`idmission`);

--
-- Indexes for table `equipements`
--
ALTER TABLE `equipements`
  ADD PRIMARY KEY (`idequipement`);

--
-- Indexes for table `installes`
--
ALTER TABLE `installes`
  ADD PRIMARY KEY (`idinstalle`),
  ADD KEY `installes_idequipement_foreign` (`idequipement`),
  ADD KEY `installes_idpoubelle_foreign` (`idpoubelle`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `missions`
--
ALTER TABLE `missions`
  ADD PRIMARY KEY (`idmission`),
  ADD KEY `missions_idvehicule_foreign` (`idvehicule`);

--
-- Indexes for table `poubelles`
--
ALTER TABLE `poubelles`
  ADD PRIMARY KEY (`idpoubelle`),
  ADD UNIQUE KEY `poubelles_numeropoubelle_unique` (`numeroPoubelle`),
  ADD KEY `poubelles_idzone_foreign` (`idzone`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idrole`);

--
-- Indexes for table `travailles`
--
ALTER TABLE `travailles`
  ADD PRIMARY KEY (`idtravail`),
  ADD KEY `travailles_iduser_foreign` (`iduser`),
  ADD KEY `travailles_idmission_foreign` (`idmission`);

--
-- Indexes for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `utilisateurs_matricule_unique` (`matricule`),
  ADD UNIQUE KEY `utilisateurs_login_unique` (`login`),
  ADD KEY `utilisateurs_idrole_foreign` (`idrole`);

--
-- Indexes for table `vehicules`
--
ALTER TABLE `vehicules`
  ADD PRIMARY KEY (`idvehicule`),
  ADD UNIQUE KEY `vehicules_immatriculation_unique` (`immatriculation`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`idzone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activites`
--
ALTER TABLE `activites`
  MODIFY `idactivite` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `concernes`
--
ALTER TABLE `concernes`
  MODIFY `idconcerne` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equipements`
--
ALTER TABLE `equipements`
  MODIFY `idequipement` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `installes`
--
ALTER TABLE `installes`
  MODIFY `idinstalle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `missions`
--
ALTER TABLE `missions`
  MODIFY `idmission` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `poubelles`
--
ALTER TABLE `poubelles`
  MODIFY `idpoubelle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `idrole` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `travailles`
--
ALTER TABLE `travailles`
  MODIFY `idtravail` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `iduser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `vehicules`
--
ALTER TABLE `vehicules`
  MODIFY `idvehicule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `idzone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activites`
--
ALTER TABLE `activites`
  ADD CONSTRAINT `activites_idequipement_foreign` FOREIGN KEY (`idequipement`) REFERENCES `equipements` (`idequipement`);

--
-- Constraints for table `concernes`
--
ALTER TABLE `concernes`
  ADD CONSTRAINT `concernes_idmission_foreign` FOREIGN KEY (`idmission`) REFERENCES `missions` (`idmission`),
  ADD CONSTRAINT `concernes_idpoubelle_foreign` FOREIGN KEY (`idpoubelle`) REFERENCES `poubelles` (`idpoubelle`);

--
-- Constraints for table `installes`
--
ALTER TABLE `installes`
  ADD CONSTRAINT `installes_idequipement_foreign` FOREIGN KEY (`idequipement`) REFERENCES `equipements` (`idequipement`),
  ADD CONSTRAINT `installes_idpoubelle_foreign` FOREIGN KEY (`idpoubelle`) REFERENCES `poubelles` (`idpoubelle`);

--
-- Constraints for table `missions`
--
ALTER TABLE `missions`
  ADD CONSTRAINT `missions_idvehicule_foreign` FOREIGN KEY (`idvehicule`) REFERENCES `vehicules` (`idvehicule`);

--
-- Constraints for table `poubelles`
--
ALTER TABLE `poubelles`
  ADD CONSTRAINT `poubelles_idzone_foreign` FOREIGN KEY (`idzone`) REFERENCES `zones` (`idzone`);

--
-- Constraints for table `travailles`
--
ALTER TABLE `travailles`
  ADD CONSTRAINT `travailles_idmission_foreign` FOREIGN KEY (`idmission`) REFERENCES `missions` (`idmission`),
  ADD CONSTRAINT `travailles_iduser_foreign` FOREIGN KEY (`iduser`) REFERENCES `utilisateurs` (`iduser`);

--
-- Constraints for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `utilisateurs_idrole_foreign` FOREIGN KEY (`idrole`) REFERENCES `roles` (`idrole`);
--
-- Database: `springboot`
--
CREATE DATABASE IF NOT EXISTS `springboot` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `springboot`;

-- --------------------------------------------------------

--
-- Table structure for table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` bigint(20) NOT NULL,
  `dateofirth` datetime DEFAULT NULL,
  `matricule` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenoms` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `etudiant`
--

INSERT INTO `etudiant` (`id`, `dateofirth`, `matricule`, `nom`, `prenoms`) VALUES
(1, '1999-09-20 00:00:00', '12345', ' YAO', 'Germain'),
(2, '1988-09-20 00:00:00', '12345', ' KOFFI', 'Amani');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Database: `toyota`
--
CREATE DATABASE IF NOT EXISTS `toyota` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `toyota`;

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2020_06_26_115514_create_cars_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Database: `youtube`
--
CREATE DATABASE IF NOT EXISTS `youtube` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `youtube`;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'ULSZ1H83iDRfsdVV0QLksLyz5w9E4jDQnrKlsz46', NULL, 'http://localhost', 1, 0, 0, '2020-07-08 09:36:38', '2020-07-08 09:36:38'),
(2, NULL, 'Laravel Password Grant Client', 'MOPL5YxmYqWJSAsdZp33Bkp77Sw0GbtGjBMSr3MT', 'users', 'http://localhost', 0, 1, 0, '2020-07-08 09:36:38', '2020-07-08 09:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-07-08 09:36:38', '2020-07-08 09:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
