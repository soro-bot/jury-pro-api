package ci.oda.jury_pro.entities;

import java.util.Date;

import javax.persistence.*;

 @Entity
 @Table(name = "evenement")
public class Evenement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="nom", length=20)
    private String nom;

    @Column(name="type", length = 50)
    private String type;

    @Column(name="dateDebut", length=15)
    private Date dateDebut;

    @Column(name="dateFin", length=15)
    private Date dateFin;

    public Integer getId() {
        return id;
    }
    public String getType() {
        return type;
    }
    public String getNom() {
        return nom;
    }
    public Date getDateDebut() {
        return dateDebut;
    }
    public Date getDateFin() {
        return dateFin;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setType(String type){
        this.type = type;
    }
    public void setNom(String nom){
        this.nom = nom;
    }
    public void setDateDebut(Date dateDebut){
        this.dateDebut = dateDebut;
    }
    public void setDateFin(Date dateFin){
        this.dateFin = dateFin;
    }

}
    
  