package ci.oda.jury_pro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ci.oda.jury_pro.entities.Groupes;
import ci.oda.jury_pro.input.GroupesInput;
import ci.oda.jury_pro.repositories.GroupesRepository;

import java.util.Base64;
import java.util.List;

@Service
public class GroupesService {

    @Autowired
    private GroupesRepository groupesRepository;

    /*
     * Recuperation de la liste
     */
    public List<Groupes> getAll() {
        return groupesRepository.findAll();
    }

    /*
    * Recuperation par identifiant
    */
    public Groupes getById(int groupe_id) {
        return groupesRepository.findById(groupe_id).orElse(null);
    }

    /*
    * Creation ou mise a jour
    */
    public boolean createOrUpdate(GroupesInput newGroupes) {
        boolean result = false;
        byte[] decodedBytes;
        try {
            if(newGroupes.getGroupe_id() != null) {
                
                if (newGroupes.getGroupe_id() > 0) {
                    Groupes item = groupesRepository.getOne(newGroupes.getGroupe_id());
                    if (item == null) {
                        // throw new Exception();
                    }
                }
            }
            if(newGroupes.getGroupe_photo().contains(",")){
                decodedBytes = Base64.getDecoder().decode(newGroupes.getGroupe_photo().split(",")[1]);
            }else{
                decodedBytes = Base64.getDecoder().decode(newGroupes.getGroupe_photo());
            }
            // Integer candidat_id, String candidat_code, String candidat_nom, String candidat_prenom, byte[] candidat_photo, String candidat_email, int candidat_telephone, int evenement_id
            groupesRepository.save(new Groupes(newGroupes.getGroupe_id(), newGroupes.getGroupe_code(), newGroupes.getGroupe_nom(),  decodedBytes, newGroupes.getEvenement_id()));
            result = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    /*
    * Suppression
    */
    public boolean delete(Integer id) {
        boolean result = false;
        Groupes item = groupesRepository.getOne(id);
        try {
            if (item == null) {
                throw new Exception();
            }
            groupesRepository.deleteById(id);
            result = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

}