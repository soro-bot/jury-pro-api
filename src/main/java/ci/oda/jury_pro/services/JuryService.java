package ci.oda.jury_pro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ci.oda.jury_pro.entities.Jury;
import ci.oda.jury_pro.repositories.JuryRepository;
import java.util.List;
import java.util.Optional;

@Service
public class JuryService {

    @Autowired
    private JuryRepository juryRepository;

    /*
     * Recuperation de la liste
     */
    public List<Jury> getAll() {
        return juryRepository.findAll();
    }

    /*
     * Recuperation par identifiant
     */
    public Jury getById(Integer jury_id) {
        return juryRepository.findById(jury_id).orElse(null);
    }

    /*
     * Creation ou mise a jour
     */
    public boolean createOrUpdate(Jury jury) {
        boolean result = false;
        try {
            if (jury.getJury_id() > 0) {
                Jury item = juryRepository.getOne(jury.getJury_id());
                // result = true;
                if (item == null) {
                    throw new Exception();
                }
            }
            juryRepository.save(jury);
            result = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    /*
     * Suppression
     */
    public boolean delete(int id) {
        boolean result = false;
        Optional<Jury> item = juryRepository.findById(id);
        try {
            if (item == null) {
                throw new Exception();
            }
            juryRepository.deleteById(id);
            result = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

}