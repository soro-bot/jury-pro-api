package ci.oda.jury_pro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ci.oda.jury_pro.entities.Candidats;
import ci.oda.jury_pro.input.CandidatInput;
import ci.oda.jury_pro.repositories.CandidatsRepository;

import java.util.Base64;
import java.util.List;

@Service
public class CandidatsService {

    @Autowired
    private CandidatsRepository candidatRepository;

    /*
     * Recuperation de la liste
     */
    public List<Candidats> getAll() {
        return candidatRepository.findAll();
    }

    /*
    * Recuperation par identifiant
    */
    public Candidats getById(int candidat_id) {
        return candidatRepository.findById(candidat_id).orElse(null);
    }

    /*
    * Creation ou mise a jour
    */
    public boolean createOrUpdate(CandidatInput newCandidat) {
        boolean result = false;
        byte[] decodedBytes;
        try {
            if(newCandidat.getCandidat_id() != null){
                if (newCandidat.getCandidat_id() > 0) {
                    Candidats item = candidatRepository.getOne(newCandidat.getCandidat_id());
                    if (item == null) {
                        // throw new Exception();
                    }
                }
            }
            if(newCandidat.getCandidat_photo().contains(",")){
                decodedBytes = Base64.getDecoder().decode(newCandidat.getCandidat_photo().split(",")[1]);
            }else{
                decodedBytes = Base64.getDecoder().decode(newCandidat.getCandidat_photo());
            }
            // Integer candidat_id, String candidat_code, String candidat_nom, String candidat_prenom, byte[] candidat_photo, String candidat_email, int candidat_telephone, int evenement_id
            candidatRepository.save(new Candidats(newCandidat.getCandidat_id(), newCandidat.getCandidat_code(), newCandidat.getCandidat_nom(), newCandidat.getCandidat_prenom(), decodedBytes, newCandidat.getCandidat_email(), newCandidat.getCandidat_telephone(), newCandidat.getEvenement_id()));
            result = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    /*
    * Suppression
    */
    public boolean delete(Integer id) {
        boolean result = false;
        Candidats item = candidatRepository.getOne(id);

        try {
            if (item == null) {
                throw new Exception();
            }
            candidatRepository.deleteById(id);
            result = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

}