package ci.oda.jury_pro.input;



public class GroupesInput {
 
    private Integer groupe_id;
    private int groupe_code;
    private String groupe_nom;
    private String groupe_photo;
    private int evenement_id;

    public GroupesInput(Integer groupes_id, int groupe_code, String groupe_nom, String groupe_photo, int evenement_id)  {
        this.groupe_id = groupes_id;
        this.groupe_code = groupe_code;
        this.groupe_nom = groupe_nom;
        this.groupe_photo = groupe_photo;
        this.evenement_id = evenement_id;
    }

    public Integer getGroupe_id() {
        return groupe_id;
    }

    public int getEvenement_id() {
        return evenement_id;
    }

    public void setEvenement_id(int evenement_id) {
        this.evenement_id = evenement_id;
    }

    public String getGroupe_photo() {
        return groupe_photo;
    }

    public void setGroupe_photo(String groupe_photo) {
        this.groupe_photo = groupe_photo;
    }

    public String getGroupe_nom() {
        return groupe_nom;
    }

    public void setGroupe_nom(String groupe_nom) {
        this.groupe_nom = groupe_nom;
    }

    public int getGroupe_code() {
        return groupe_code;
    }

    public void setGroupe_code(int groupe_code) {
        this.groupe_code = groupe_code;
    }

    public void setGroupe_id(int groupe_id) {
        this.groupe_id = groupe_id;
    }

    public GroupesInput() {
    }

}
